QT += core

TARGET = AsciiArt
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    asciiart.cpp

HEADERS += \
    asciiart.h

INCLUDEPATH+=/usr/include
LIBS+=-lopencv_core -lopencv_highgui 
