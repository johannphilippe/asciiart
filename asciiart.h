#ifndef ASCIIART_H
#define ASCIIART_H

#include<opencv2/opencv.hpp>
#include<opencv2/opencv_modules.hpp>
#include<vector>
#include<string>
#include<iostream>
#include<QtGui/QImage>
#include<QtGui/QColor>


#define RGB (3)
#define RGBA (4)

using namespace std;
using namespace cv;


class AsciiArt
{
public:
        AsciiArt(int h=0,int w=0);
        virtual ~AsciiArt();

        void setHeight(int h);
        void setWidth(int w);
        int setVector(unsigned char* imgArr,int rgba);
        void convertToAscii(int hF,int wF, int algo = 1);
        int fromImg(const char *path);
        void clearAll();
        string getString();

        char asciiAlgo(int val,int algoIdx = 1);

        void rotateVector(int quarter = 1);




    protected:

    private:

    //private methods
    int findMax(vector<int> *arr,int s);
    int average(unsigned char *arr,int s);
    template<class T> int avg(T *arr,int s);

    //fields
    int height,width;
    vector< vector<int> > img;
    string asciiString;

};


#endif // ASCIIART_H
