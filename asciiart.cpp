#include "asciiart.h"


AsciiArt::AsciiArt(int h,int w)
{
//    height=h;
//    width=w;
    //ctor
}

AsciiArt::~AsciiArt()
{
    //dtor
}

//those three methods useful if image is passed as data rather than binary file
void AsciiArt::setHeight(int h) {height=h;}

void AsciiArt::setWidth(int w) {width=w;}

int AsciiArt::setVector(unsigned char* imgArr,int rgba)
{
img.clear();
//rgba =4 or 3 (ignore or not 4th)
int cnt=0;
int res=0;
    for(int h=0;h<height;h++) {
        vector <int> v;
        for(int w=0;w<width;w++) {
            unsigned char temp[3];
            for (int i=0;i<3;i++) {temp[i]=imgArr[cnt+i];}
            v.push_back(average(temp,3));
            cnt+=rgba;
        }
        img.push_back(v);
    }
    return res;
}



//converting to ascii chain
void AsciiArt::convertToAscii(int hF,int wF, int algo){
    asciiString.clear();
    for(int h=0;h<img.size();h+=hF) {
        for(int w=0;w<img[h].size();w+=wF){
            char cur=asciiAlgo(img[h][w],algo);
            asciiString.push_back(cur);
        }
        asciiString.push_back('\n');
    }
}


void AsciiArt::clearAll() {
    asciiString.clear();
    img.clear();
    height=0;
    width=0;
}

int AsciiArt::fromImg(const char *path) {
    QImage image(path);
    if(image.isNull()) {
        cout << "cannot read image" << endl;
        return -1;
    } else {

    height=image.height();
    width=image.width();
    img.clear();

    for(int h=0;h<height;h++) {
        vector<int> temp;
        for(int w=0;w<image.width();w++) {
            QColor clr(image.pixel(h,w));
            //QRgb clr=image.pixel(h,w);

            int px[3]={clr.red(),clr.blue(),clr.green()};
            int pix=avg(px,3);
            temp.push_back(pix);
        }
        img.push_back(temp);
        cout << temp.size() << endl;
    }


    return 0;
    }


}


string AsciiArt::getString() {
return asciiString;
}

char AsciiArt::asciiAlgo(int val,int algoIdx) {
    char res;
    switch(algoIdx) {
    case 1 :
        if(val>=0 && val<20) {res=' ';}
        else if(val>=20 && val<60) {res='.';}
        else if(val>=60 && val <120) {res='|';}
        else if(val>=120 && val<180) {res='@';}
        else if(val>=180) {res='#';}
        break;
    case 2 :
        if(val>=0 && val<20) {res=' ';}
        else if(val>=20 && val<40) {res='.';}
        else if(val>=40 && val<60) {res=':';}
        else if(val>=60 && val < 80) {res=';';}
        else if(val>=80 && val <100) {res='!';}
        else if(val>=100 && val <120) {res='|';}
        else if(val>=120 && val<140) {res='=';}
        else if(val>=140 && val < 160) {res='%';}
        else if(val>=160 && val<180) {res='£';}
        else if(val>=180 && val < 200) {res='$';}
        else if(val>200 && val<220) {res='@';}
        else if(val>=220) {res='#';}
        break;
    }

    return res;
}






int AsciiArt::findMax(vector<int> *arr,int s) {
    int m=0;
    for(int i=0;i<s;i++) {
        if(arr->at(i)>m) {m=arr->at(i);}
    }
    return m;
}

int AsciiArt::average(unsigned char arr[3],int s) {
    int res=0;
    for(int i=0;i<s;i++) {
        res+=(int)arr[i];
    }
    res/=s;
    return (int)res;
}

template<class T> int AsciiArt::avg(T *arr,int s) {
    int res=0;
    for(int i=0;i<s;i++) {
        res+=(int)arr[i];
    }
    res/=s;
    return (int)res;


}


void AsciiArt::rotateVector(int quarter) {
        vector<vector<int> > tempImg;
        bool hasChanged=false;
       switch(quarter) {
       //90 degrees clock way
        case 1 :
           hasChanged=true;
           for(int w=0;w<img[0].size();w++) {
              vector<int> temp;
              for(int h=img.size()-1;h>=0;h--) {
                  temp.push_back(img.at(h).at(w));
              }
              tempImg.push_back(temp);
           }
           break;
        //180 degrees
        case 2 :
           hasChanged=true;
            for(int h=img.size()-1;h>=0;h--) {
                vector<int> temp;
                for(int w=img.size()-1;w>=0;w--) {
                    temp.push_back(img.at(h).at(w));
                }
                tempImg.push_back(temp);
            }
           break;
        //270 degrees
        case 3:
           hasChanged=true;
           for(int w=img[0].size()-1;w>=0;w--) {
               vector<int> temp;
               for(int h=0;h<img.size();h++) {
                   temp.push_back(img.at(h).at(w));
               }
               tempImg.push_back(temp);
           }
           break;
        //mirror horizontal
       case 4 :
           hasChanged=true;
           for(int h = 0;h<img.size();h++) {
               vector<int> temp;
               for(int w=img[h].size()-1;w>=0;w--) {
                   temp.push_back(img[h][w]);
               }
               tempImg.push_back(temp);
           }
           break;
        default : hasChanged=false;
       }

       if (hasChanged==true) {
        img.clear();
        img=tempImg;
       }
}

